import React from 'react'
import styled from 'styled-components';

const StyledTable = styled.table`
  width: 70%;
  border: 2px solid black;
  margin: auto;
  font-size: 20px;
  text-align: center;
  margin-top: 20px;
  table-layout: fixed;
`;

const StyledTd = styled.td`
  padding: 10px;
  white-space: pre-wrap;
  border: 1px solid black;
`;

const FirstRowStyledTd = styled(StyledTd)`
  background-color: #f2f2f2;
  border-top: 2px solid black;
`;

const Row = ({ attribute, details, additionalInfo, isHeader }) => (
  <tr>
    {isHeader ? (
      <FirstRowStyledTd>{attribute}</FirstRowStyledTd>
    ) : (
      <StyledTd>{attribute}</StyledTd>
    )}
    <StyledTd>{details}</StyledTd>
    <StyledTd>{additionalInfo}</StyledTd>
  </tr>
);

class ExportPDF extends React.PureComponent {
  cellStyleNormal = {
    border: '1px solid black',
    padding: '10px',
    whiteSpace: 'nowrap',
  };
  cellStyleLightGrey = {
    ...this.cellStyleNormal,
    backgroundColor: '#f2f2f2',
  };
  cellStyleDarkGrey = {
    ...this.cellStyleNormal,
    backgroundColor: 'lightgrey',
  };

  cellStyleWinter = {
    ...this.cellStyleNormal,
    backgroundColor: '#e6f7ff',
  };

  cellStyleSummer = {
    ...this.cellStyleNormal,
    backgroundColor: '#ffe6cc',
  };

  render() {

    const { courses, semestersToShow } = this.props

    console.log(semestersToShow)
    console.log(courses)

    const nazvy = courses.map(kurz => {return kurz.name})
    console.log(nazvy)

    // const prvnirok = courses.filter(kurz => kurz.position === "1-w" || kurz.position === "1-s")
    const prvnirok = courses.filter(kurz => kurz.position.includes("1-"))
    console.log(prvnirok)

    const renderprvnirok =  prvnirok.map((kurz,index) => { return(
      <tr key={index}><td>{kurz.name}</td><td>{kurz.semester}</td><td>{kurz.training}</td></tr>
    )})

    return (

      <div ref={this.props.forwardedRef} style={{ marginTop: '30px' }}
        // inline styles in react / styled-components
      >
        <h1 className='Your Curricula Table' style={{textAlign:'center'}} >Your Curricula</h1>

        <table>
          <tbody>
            {renderprvnirok}
          </tbody>
        </table>

        <table
          style={{
            width: '50%',
            height: '50%',
            border: '1px solid black',
            margin: 'auto',
            fontSize: '30px',
            textAlign: 'center',
          }}
        >
          <tbody>
            <tr>
              <td style={this.cellStyleLightGrey}></td>
              <td colSpan="2" style={this.cellStyleLightGrey}>Semesters</td>
            </tr>
            <tr>
              <td style={this.cellStyleLightGrey}></td>
              <td style={this.cellStyleWinter}>winer</td>
              <td style={this.cellStyleSummer}>summer</td>
            </tr>
            <tr>
              <td style={this.cellStyleLightGrey}>1st</td>
              <td style={this.cellStyleNormal}></td>
              <td style={this.cellStyleNormal}></td>
            </tr>
            <tr>
              <td style={this.cellStyleLightGrey}>2nd</td>
              <td style={this.cellStyleNormal}></td>
              <td style={this.cellStyleNormal}></td>
            </tr>
            <tr>
              <td style={this.cellStyleLightGrey}>4th</td>
              <td style={this.cellStyleNormal}></td>
              <td style={this.cellStyleNormal}></td>
            </tr>
            <tr>
              <td style={this.cellStyleLightGrey}>5th</td>
              <td style={this.cellStyleNormal}></td>
              <td style={this.cellStyleNormal}></td>
            </tr>
              <tr>
              <td style={this.cellStyleLightGrey}>6th</td>
              <td style={this.cellStyleNormal}></td>
              <td style={this.cellStyleNormal}></td>
            </tr>
          </tbody>
        </table>
{/* ------------------------------------------------------------------------------------ */}
        <table
          style={{
            width: '70%',
            border: '1px solid black',
            margin: 'auto',
            fontSize: '30px',
            textAlign: 'center',
            marginTop: '30px',
          }}
        >
          <tbody>
            <tr>
              <td style={this.cellStyleDarkGrey}>Name</td>
              <td style={this.cellStyleDarkGrey}>Type</td>
              <td style={this.cellStyleDarkGrey}>Semester</td>
              <td style={this.cellStyleDarkGrey}>Training</td>
              <td style={this.cellStyleDarkGrey}>ETCS credits</td>
            </tr>

            {[...Array(10)].map((_, index) => (
              <tr key={index}>
                <td style={index % 2 === 0 ? this.cellStyleNormal : this.cellStyleLightGrey}></td>
                <td style={index % 2 === 0 ? this.cellStyleNormal : this.cellStyleLightGrey}></td>
                <td style={index % 2 === 0 ? this.cellStyleNormal : this.cellStyleLightGrey}></td>
                <td style={index % 2 === 0 ? this.cellStyleNormal : this.cellStyleLightGrey}></td>
                <td style={index % 2 === 0 ? this.cellStyleNormal : this.cellStyleLightGrey}></td>
              </tr>
            ))}
          </tbody>
        </table>
{/* ------------------------------------------------------------------------------------ */}
        <StyledTable>
          <tbody>
            <tr>
              <FirstRowStyledTd>Attribute</FirstRowStyledTd>
              <FirstRowStyledTd>Details</FirstRowStyledTd>
              <FirstRowStyledTd>Additional Information</FirstRowStyledTd>
            </tr>
            <Row
              attribute="Name"
              details="Computers and Programming 2"
              additionalInfo="-"
            />
            <Row attribute="Type" details="Mandatory" additionalInfo="-" />
            <Row attribute="Semester" details="Summer" additionalInfo="-" />
            <Row attribute="Training" details="Yes" additionalInfo="-" />
            <Row attribute="ETCS credits" details="5" additionalInfo="-" />
            <Row
              attribute="Topics (ETCS %)"
              details="Software Engineering"
              additionalInfo="-"
            />
            <Row
              attribute="ECSF Skills"
              details="Assess the security and performance of solutions, Collaborate with other team members and colleagues, Communicate, present and report to relevant stakeholders"
              additionalInfo="-"
            />
            <Row
              attribute="ECSF Knowledge"
              details="Computer networks security, Computer programming, Cybersecurity controls and solutions"
              additionalInfo="-"
            />
            <Row
              attribute="Non-ECSF Skill/Knowledge"
              details="N/A"
              additionalInfo="-"
            />
          </tbody>
        </StyledTable>
      </div>
    )
  }
}

export default React.forwardRef((props, ref) => <ExportPDF {...props} forwardedRef={ref} />)
