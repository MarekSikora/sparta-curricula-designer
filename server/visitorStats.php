<?php
header("Access-Control-Allow-Origin: *");

include("config.php");

$output = new stdClass();
$output->error = false;
$output->message = "ok";	

try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare("SELECT feedback FROM visitors");
	$stmt->execute();
	
	$output->data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
} catch(PDOException $e) {
	$output->error = true;
	$output->message = "Error: " . $e->getMessage();
}

$conn = null;
echo $json_string = json_encode($output, JSON_PRETTY_PRINT);


?>

