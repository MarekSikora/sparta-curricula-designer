<?php
header("Access-Control-Allow-Origin: *");

include("config.php");

$fingerprint = $_GET["a"];
$output = new stdClass();
$output->error = false;
	
if (isset($_GET["a"]) && isset($_GET["b"])){
	
	$output->message = "ok";	
	try {
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			// update feedback answer
		$feedback = $_GET["b"] == "true" ? "useful" : ($_GET["b"] == "false" ? "useless" : "");
		$stmt = $conn->prepare("UPDATE visitors SET feedback = :feedback WHERE fingerprint = :fingerprint");
		$stmt->bindParam(':feedback', $feedback);
		$stmt->bindParam(':fingerprint', $fingerprint);
		$stmt->execute();
			
	} catch(PDOException $e) {
		$output->error = true;
		$output->message = "Error: " . $e->getMessage();
	}
	
	$conn = null;
	echo $json_string = json_encode($output, JSON_PRETTY_PRINT);

} else if (isset($_GET["a"])){
	
	$output->message = "none";
	try {
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("SELECT id, feedback, last_visit, visit_counter FROM visitors WHERE fingerprint = :fingerprint");
		$stmt->bindParam(':fingerprint', $fingerprint);
		$stmt->execute();
		
		if ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$output->message = $result["feedback"];
			
			// update visit counter
			$stmt = $conn->prepare("UPDATE visitors SET visit_counter = :new_counter, last_visit = now() WHERE id = :id");
			$stmt->bindParam(':id', $result["id"]);
			$stmt->bindParam(':new_counter', $new_counter );
			$new_counter = (int)$result["visit_counter"] + 1;
			$stmt->execute();
			
		} else {
			
			// insert new visitor
			$stmt = $conn->prepare("INSERT INTO visitors (fingerprint, visit_counter) VALUES(:fingerprint, 1)");
			$stmt->bindParam(':fingerprint', $fingerprint);
			$stmt->execute();
			
		}

	} catch(PDOException $e) {
		$output->error = true;
		$output->message = "Error: " . $e->getMessage();
	}
	
	$conn = null;
	echo $json_string = json_encode($output, JSON_PRETTY_PRINT);

} else {
	http_response_code(403);
}

?>

