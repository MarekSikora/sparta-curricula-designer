# Cybersecurity Curricula Designer

This project aims at minimizing the skills gaps and skills shortages on the cybersecurity job market by empowering education and training institutions during the process of creation of new cybersecurity study programs. We provide a complex cybersecurity skills framework based on standardized definitions that helps with the identification of skills and knowledge necessary for cybersecurity work positions. Furthermore, we practically implement the framework in the form of an interactive web application for cybersecurity curricula design.

The app, called Curricula Designer, is built upon the framework and allows intuitive design of higher-education curricula and their analysis with respect to requirements of work roles already defined in widely-accepted standards. Using the analytical functions, it is easy to identify missing content in the courses and precisely structure the study program so that the graduates are well-prepared to enter the job market.


## Features

* Compilation of a study plan from study courses.
* Creating your own courses, editing courses.
* Overview of ECTS credits, fulfilling competencies, available work roles.
* Save the created study plan to a file.
* Load a saved study plan from a file.
* User manual.
* Log of visitors.
* Feedback questionnaire.


## Server app

A separate PHP server application is used to store visitor records. Each visitor record contains SHA256 hash of a string composed of IP address information and browser fingerprint. If the application recognizes that the user has not yet provided feedback, the application will ask him after an activity if this application was useful for him. The response is sent and stored in the server application. Collected data can be viewed on the `/visitor-stats` subpage.


## Technologies

* Javascript
* CSS

### Libraries

* react
* react-router
* jquery
* sass
* bootstrap
* beautiful-dnd
* axios
* apexcharts
* recharts
* material-ui
* fingerprintjs2
* crypto
* popper.js
* react-tooltip
* styled-components
* typescript


## Installation

Use the package manager [npm](https://www.npmjs.com/) to install the app.

```bash
npm install
```


## App Configuration

App root folder and the server app URL could be configured in `/src/config.js` file.

Production homepage URL could be configured in `/package.json` file.


## Run in Development Mode

```bash
npm start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.


## Production Build

Correct production homepage URL has to be set in `/package.json` file.

```bash
npm run build
```

Builds the app for production to the `/build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
The app is ready to be deployed!


## Live Demo

Fully functional demo is available here: [https://curricula-designer.informacni-bezpecnost.cz](https://curricula-designer.informacni-bezpecnost.cz)


## Official release

The application is officially deployed within the SPARTA project at [https://www.sparta.eu/curricula-designer/](https://www.sparta.eu/curricula-designer/).


## License

Copyright 2021 Brno University of Technology

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
