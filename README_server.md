# Cybersecurity Curricula Designer - Server App

This project aims at minimizing the skills gaps and skills shortages on the cybersecurity job market by empowering education and training institutions during the process of creation of new cybersecurity study programs. We provide a complex cybersecurity skills framework based on standardized definitions that helps with the identification of skills and knowledge necessary for cybersecurity work positions. Furthermore, we practically implement the framework in the form of an interactive web application for cybersecurity curricula design.
<br />

The app, called Curricula Designer, is built upon the framework and allows intuitive design of higher-education curricula and their analysis with respect to requirements of work roles already defined in widely-accepted standards. Using the analytical functions, it is easy to identify missing content in the courses and precisely structure the study program so that the graduates are well-prepared to enter the job market.
<br />

Curricula Designer App is available here: [https://bitbucket.org/MarekSikora/curricula-designer/src](https://bitbucket.org/MarekSikora/curricula-designer/src)


## Server app

This support server provides storage of visit data provided by the main Javascript application. Records include client fingerprint hash, feedback response, number of visits, and time of last visit. The data is stored in a MySQL database, which in the original concept is installed on the same machine as this server.


## Technologies

* PHP
* MySQL


## Installation

Copy files to your web server.


## App Configuration

MySQL credentials could be configured in `/src/config.js` file.
